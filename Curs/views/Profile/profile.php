<?php
/**
 * Контент сторінки профілю
 */
?>

<main class="container-fluid">
    <h1>Профиль</h1>
    <p id="success"><?=$Success?></p>
    <form method="post" action="profile">
        <table>
            <tr>
                <td><label>Имя:</label></td>
                <td><input type="text" name="Name" value="<? echo $UserData[1]; ?>"></td>
            </tr>
            <tr>
                <td><label>Фамилия:</label></td>
                <td><input type="text" name="Surname" value="<? echo $UserData[2]; ?>"></td>
            </tr>
            <tr>
                <td><label>E-Mail:</label></td>
                <td><input type="email" name="EMail" value="<? echo $UserData[3]; ?>"></td>
            </tr>
            <tr>
                <td><label>Страна:</label></td>
                <td><input type="text" name="Country" value="<? echo $UserData[4]; ?>"></td>
            </tr>
            <tr>
                <td><label>Логин:</label></td>
                <td><input type="text" name="Login" value="<? echo $UserData[6]; ?>"></td>
            </tr>
            <tr>
                <td><label>Дата рождения:</label></td>
                <td><input type="date" name="Date_of_birth" value="<? echo $UserData[7]; ?>"></td>
            </tr>
            <tr>
                <td><label>Стать:</label></td>
                <td><label><input type="radio" name="Sex" value="мужчина" checked>мужчина</label>
                    <label><input type="radio" name="Sex" value="женщина" <? if ($UserData[8] == 'женщина') echo 'checked'; ?>>женщина</label>
                </td>
            </tr>
            <tr>
                <td><label>О себе:</label></td>
                <td><textarea name="Hobbies"><? echo $UserData[9]; ?></textarea></td>
            </tr>
            <tr>
                <td><label>Пароль:</label><p id="hint">Оставьте пустым,<br>если не желаете изменить</p></td>
                <td><input type="password" name="Password"></td>
            </tr>
            <tr>
                <td><label>Повторите пароль:</label></td>
                <td><input type="password" name="pass2"></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button name="change" type="submit" value="Зберегти зміни" class="btn btn-outline-primary">Сохранить
                        изменения
                    </button>
                </td>
            </tr>
        </table>
    </form>
    <br>
    <form class="delete" method="post" action="profile">
        <button name="deleteButton" type="submit" class="btn btn-outline-primary">Удалить профиль</button>
    </form>
    <div id="error">
        <? echo $ErrorProfile ?>
    </div>
</main>
