<?php
/**
 * Контент корзини
 */
?>
<main class="container">
    <? if(!$Cart){
        echo "
<div class='col-12'>
<p class='empty'>Ваша корзина пока пуста</p>
</div>";
    }
    else {
        $totalPrice = 0;
        foreach ($Cart as $item) {
            echo "<div class='row cartItem'>
<div class='col-4 col-md-2'>
<img src='{$item['Img']}'>
</div>
<div class='col-6 col-md-8'>
<a href='book?id={$item['id_book']}' class='text-dark'>
<p class='nameBook'>{$item['Bookname']}</p>
<p>{$item['Author']}</p>
<p class='price'>{$item['Price']}$</p>
</a>
<form action='cart?id={$item['id_book']}' method='post'>
Количество: <input type='number' class='count' name='count' value='{$item['Count']}' min='1' max='99' step='1' required></input><br>
<button class='btn btn-outline-primary' type='submit' name='changeCount'>Изменить</button>
</form>
</div>
<div class='col-1 offset-md-1'>
<form action='cart?id={$item['id_book']}' method='post'>
<button class='btn btn-outline-danger' type='submit' name='deleteBook'>
<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"24\" viewBox=\"0 0 23 25\" width=\"24\"><path d=\"M0 0h24v24H0V0z\" fill=\"none\"/><path d=\"M14.59 8L12 10.59 9.41 8 8 9.41 10.59 12 8 14.59 9.41 16 12 13.41 14.59 16 16 14.59 13.41 12 16 9.41 14.59 8zM12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z\"/></svg>
</button>
</form>
</div>
</div>";
            $totalPrice += $item['Price'] * $item['Count'];
        }
        echo "<p class='total'>Итого: {$totalPrice}$</p>";
    }?>
</main>
