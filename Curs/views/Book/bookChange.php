<?php
/**
 * Контент сторінки книжки в режимі редагування
 */
?>
<main class="container">
    <div class="row justify-content-center">
        <div class="col-11 col-md-4">
            <img class='bookImage' src="<?= $Book['Img'] ?>">
        </div>
        <form class="col-11 col-md-8 changeForm d-flex" method='post' action='book?id=<?= $Book['id_book'] ?>' enctype="multipart/form-data">
            <input type='text' name='Bookname' class='change' value="<?= $Book['Bookname'] ?>">
            <input type='text' name='Author' class='change' value="<?= $Book['Author'] ?>">
            <input type='number' name='Price' class='change' value="<?= $Book['Price'] ?>">
            <textarea name='Description' class="desc"><?= $Book['Description'] ?></textarea>
            <input type="file" name="Img">
            <button class='btn btn-outline-primary saveButton' name='save' type='submit'>Сохранить</button>
        </form>
    </div>
</main>
<form action='book?id=<?= $Book['id_book'] ?>' method='post' class='changeButtonForm'>
    <button name='delete' class='btn btn-outline-danger'>Удалить</button>
</form>