<?php
/**
 * Контент сторінки книжки
 */
?>
<main class="container">
<div class="row justify-content-center">
    <div class="col-11 col-md-4">
        <img class='bookImage' src="<?=$Book['Img']?>">
    </div>
    <div class="col-11 col-md-8">
        <p class="name"><?=$Book['Bookname']?></p>
        <p class="author"><?=$Book['Author']?></p>
        <p class="price"><?=$Book['Price']?>$</p>
        <p class="desc"><?=$Book['Description']?></p>
        <form action='book?id=<?=$Book['id_book']?>' method='post'>
            <button class='btn btn-outline-success' name='buy'><img src='images/shopping_cart_PNG3.png' class='cart'>&nbsp В КОРЗИНУ</button>
            <input type='number' value='1' name='countOrder' class='count' min='1' max='99' step='1' required>
        </form>
        <p class='error'><?=$ErrorBuy?></p>
    </div>
</div>
</main>
<? if($isAdmin){
    echo "<form action='book?id={$Book['id_book']}' method='post' class='changeButtonForm'>
    <button class='btn btn-outline-primary' name='change' type='submit'>Изменить</button>
</form>";
} ?>


