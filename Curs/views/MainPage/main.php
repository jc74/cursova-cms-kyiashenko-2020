<?php
/**
 * Контент головної сторінки
 */
?>
<main class="container-fluid">
<h2>Бестселлеры</h2>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="../../images/witcher.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="../../images/harry.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="../../images/mark.jpg" class="d-block w-100" alt="...">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div id="new">
    <h3>Новинки</h3>
    <div class="container">
        <div class="row justify-content-center">
            <div class='row justify-content-center'>
                <?php
                $i = 0;
                foreach ($newBooks as $item) {
                    echo "
        <div class='col-11 col-md-2 book m-4  align-self-center'>
        <a class='text-white' href='book?id={$item['id_book']}'>
        <img class='bookImage' src='{$item['Img']}'>
        <div class='row'>
        <div class='col-12'>
<p class=\"nameBook\">{$item['Bookname']}
{$item['Author']}</p>
<p class=\"price\">{$item['Price']}$</p>
</div>
</div>
</a>
</div>";
                    $i++;
                    if ($i > 3)
                        break;
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div id="soon">
    <h3>СКОРО</h3>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11 col-md-4">
                <p>Презентация книги Мой собственный Париж</p>
                <p>Под редакцией Элеоноры Браун</p>
                <p class="bold">Когда</p>
                <hr>
                <p>11 Июня 2020 года, 17:00</p>
                <p class="bold">Где</p>
                <hr>
                <p>Житомирская политехника</p>
            </div>
            <div class="col-11 col-md-4 offset-md-1 bookBorder">
                <img src="../../images/books/i695237.jpg">
            </div>
        </div>
    </div>
</div>
</main>