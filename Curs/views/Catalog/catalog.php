<?php
/**
 * Контент сторінки каталог
 */
?>
<main class="container">
    <? if($isAdmin) {
        echo "<form action='catalog' method='post' class='changeButtonForm'>
    <button class='btn btn-outline-primary' name='add' type='submit'>Добавить</button>
</form>";
    }?>
<div class='row justify-content-center'>
    <?php
    foreach ($Books as $item) {
        echo "
        <div class='col-4 col-md-2 book m-4  align-self-center'>
        <a class='text-dark' href='book?id={$item['id_book']}'>
        <img class='bookImage' src='{$item['Img']}'>
        <div class='row'>
        <div class='col-12'>
<p class=\"nameBook\">{$item['Bookname']}
{$item['Author']}</p>
<p class=\"price\">{$item['Price']}$</p>
</div>
</div>
</a>
</div>

        ";
    }
    ?>
</div>
</main>
