<?php
/**
 * Created by PhpStorm.
 * User: Анатолий
 * Date: 11.06.2020
 * Time: 7:47
 */
?>
<main class="container">
    <p class="success"><?=$Success?></p>
    <div class="row justify-content-center">
        <form class="col-11 changeForm d-flex" method='post' action='catalog' enctype="multipart/form-data">
            Название книги: <input type='text' name='Bookname' class='change' value="<?=$Inserted['Bookname']?>">
            Автор: <input type='text' name='Author' class='change' value="<?=$Inserted['Author']?>">
            Цена: <input type='number' name='Price' class='change' value="<?=$Inserted['Price']?>">
            Описание: <textarea name='Description' class="desc"><?=$Inserted['Description']?></textarea>
            Изображение: <input type="file" name="Img">
            <button class='btn btn-outline-primary saveButton' name='save' type='submit'>Сохранить</button>
        </form>
    </div>
</main>