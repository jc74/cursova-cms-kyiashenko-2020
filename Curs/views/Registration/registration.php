<?php
/**
 * Контент сторінки регістрації
 */
?>
<main class="container-fluid">
    <h1>Регистрация</h1>
    <form method="post" action="registration">
        <table>
            <tr>
                <td><label>Логин:</label></td>
                <td><input type="text" name="Login" value="<?=$regData['login'] ?>"></td>
            </tr>
            <tr>
                <td><label>Пароль:</label></td>
                <td><input type="password" name="Password"></td>
            </tr>
            <tr>
                <td><label>Повторите пароль:</label></td>
                <td><input type="password" name="pass2"></td>
            </tr>
            <tr>
                <td><label>E-Mail:</label></td>
                <td><input type="email" name="EMail" value="<?=$regData['email'] ?>"></td>
            </tr>
            <tr>
                <td><label>Страна: </label></td>
                <td><input type="text" name="Country" value="<?=$regData['country'] ?>"></td>
            </tr>
            <tr>
                <td><label>Имя:</label></td>
                <td><input type="text" name="Name" value="<?=$regData['name'] ?>"></td>
            </tr>
            <tr>
                <td><label>Фамилия:</label></td>
                <td><input type="text" name="Surname" value="<?=$regData['surname'] ?>"></td>
            </tr>
            <tr>
                <td><label>Пол:</label></td>
                <td><label><input type="radio" name="Sex" value="мужчина" checked>мужчина</label>
                    <label><input type="radio" name="Sex" value="женщина" <? if ($regData['sex'] == 'женщина') echo 'checked'; ?>>женщина</label>
                </td>
            </tr>
            <tr>
                <td><label>Дата рождения:</label></td>
                <td><input type="date" name="Date_of_birth" value="<?=$regData['dateOfBirth'] ?>"></td>
            </tr>
            <tr>
                <td><label>О себе: </label></td>
                <td><textarea name="Hobbies" placeholder="Опционально"><?=$regData['hobbies'] ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button type="submit" value="reg" class="btn btn-outline-primary">Зарегистрироваться</button>
                </td>
            </tr>
        </table>
    </form>
    <div id="error">
        <?=$regData['error'] ?>
    </div>
</div>
</main>