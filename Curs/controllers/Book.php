<?php
/**
 * Контролер сторінки книжки
 */

namespace controllers;


use core\Controller;

class Book extends Controller
{
    protected $BookId;
    protected $Book = [];
    protected $UpdateBook = [];

    public function getCss()
    {
        $this->css = "css/bookStyle.css";
    }

    public function addToCart($idBook)
    {
            $errorBuy = '';
            $idUser = $this->model->getIdUser($this->login);
            if(is_null($idUser)){
                $errorBuy = "Для покупки необходимо зарегистрироваться или войти";
            }
            else {
                $this->model->addToCart($idUser, $idBook);
            }
            return $errorBuy;
    }

    function saveChanges($idBook)
    {
            $error = '';
            $img = '';
            $this->UpdateBook['Bookname'] = $this->model->test_input($_POST['Bookname']);
            $this->UpdateBook['Author'] = $this->model->test_input($_POST['Author']);
            $this->UpdateBook['Price'] = $this->model->test_input($_POST['Price']);
            $this->UpdateBook['Description'] = $this->model->test_input($_POST['Description']);

            foreach ($this->UpdateBook as $value) {
                if ($value == '') {
                    $error = "Заполните все обязательные поля";
                    break;
                }
            }
            $img = $_FILES['Img']['name'];
            $this->error = $error;
            if ($error == '') {
                $this->model->updateBook($idBook);
                if ($img != '')
                {
                    $this->model->updateImage($idBook);
                }
                header("Refresh: 0");
            }
    }

    function deleteBook($idBook)
    {
        $this->model->deleteBook($idBook);
        header('Location: Catalog');
        exit();
    }

    public function actionIndex()
    {
        $error = '';
        $this->BookId = $_GET['id'];
        if(isset($_POST['buy'])) {
            $error = $this->addToCart($this->BookId);
        }
        $this->Book = $this->model->getBook($this->BookId);
        if(isset($_POST['save'])) {
            $this->saveChanges($this->BookId);
            return $this->render('bookChange', ['Book' => $this->Book, 'isAdmin' => $this->isAdmin()], ['Title' => $this->Book['Bookname'], 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
        }
        if(isset($_POST['delete'])){
            $this->deleteBook($this->BookId);
        }
        if(isset($_POST['change'])){
            return $this->render('bookChange', ['Book' => $this->Book, 'isAdmin' => $this->isAdmin()], ['Title' => $this->Book['Bookname'], 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
        }
        if ($this->Book) {
            return $this->render('book', ['Book' => $this->Book, 'ErrorBuy' => $error, 'isAdmin' => $this->isAdmin()], ['Title' => $this->Book['Bookname'], 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
        }
        else{
            header("Location: Error404");
        }
    }

}