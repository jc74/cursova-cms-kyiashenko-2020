<?php
/**
 * Контролер сторінки регістрації
 */

namespace controllers;


use core\Controller;

class registration extends Controller
{
    protected $RegData = [];

    public function getCss()
    {
        $this->css = "css/registrationStyle.css";
    }

    public function getEnterForm()
    {
        $this->enterForm = '';
    }

    public function checkRegData()
    {
        $error = '';
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $this->RegData['login'] = $this->model->test_input($_POST['Login']);
            $this->RegData['pass'] = $this->model->test_input($_POST['Password']);
            $this->RegData['pass2'] = $this->model->test_input($_POST['pass2']);
            $this->RegData['email'] = $this->model->test_input($_POST['EMail']);
            $this->RegData['sex'] = $this->model->test_input($_POST['Sex']);
            $this->RegData['country'] = $this->model->test_input($_POST['Country']);
            $this->RegData['name'] = $this->model->test_input($_POST['Name']);
            $this->RegData['surname'] = $this->model->test_input($_POST['Surname']);
            $this->RegData['dateOfBirth'] = htmlentities($_POST['Date_of_birth']);

            if (!preg_match($this->RegExPass, $this->RegData['pass'])) {
                $error = "Некоректная форма пароля";
            }
            if ($this->RegData['pass'] != $this->RegData['pass2']) {
                $error = "Пароли не совпадают";
            }
            foreach ($this->RegData as $value) {
                if ($value == "") {
                    $error = "Заполните все обязательные поля";
                    break;
                }
            }
            if ($error != "") {
                $this->RegData['error'] = $error;
            }
            else {
                $this->RegData['error'] = $this->model->registration($this->RegData);
            }
        }
    }

    public function actionIndex()
    {
        $this->checkRegData();
        if (isset($this->RegData['error']) && $this->RegData['error'] == '') {
            header('location: /');
            exit();
        }
        return $this->render('registration', ['regData' => $this->RegData], ['Title' => 'Регистрация', 'EnterForm' => $this->enterForm, 'Css' => $this->css]);
    }

}