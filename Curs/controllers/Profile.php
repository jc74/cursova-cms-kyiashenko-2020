<?php
/**
 * Контролер сторінки профілю
 */

namespace controllers;


use core\Controller;

class Profile extends Controller
{
    protected $UserData = [];
    protected $Success = '';

    public function getCss()
    {
        $this->css = "css/profileStyle.css";
    }

    public function getProfileData($idUser)
    {
        $this->UserData = $this->model->getUserData($idUser);
    }

    public function changeUserData($idUser)
    {
        $correct = false;
        $newLogin = $this->model->test_input($_POST['Login']);
        $pass = $this->model->test_input($_POST['Password']);
        $pass2 = $this->model->test_input($_POST['pass2']);
        $email = $this->model->test_input($_POST['EMail']);
        $country = $this->model->test_input($_POST['Country']);
        $name = $this->model->test_input($_POST['Name']);
        $surname = $this->model->test_input($_POST['Surname']);
        $dateOfBirth = htmlentities($_POST['Date_of_birth']);
        $error = "";

        if ($newLogin != "" && $email != "" &&
            $name != "" && $surname != "" &&
            $dateOfBirth != "" && $country != "") {
            if ($newLogin != $this->login) {
                $resLog = $this->model->getUserByLogin($newLogin);
                if ($resLog) {
                    $error = "Пользователь с таким логином уже есть";
                    return $error;
                }
            }
            if ($pass != '') {
                if (!preg_match($this->RegExPass, $pass)) {
                    $error = "Некорректная форма пароля";
                    return $error;
                } else if ($pass != $pass2) {
                    $error = "Пароли не совпадают";
                    return $error;
                } else {
                    $this->model->updatePassword($idUser, $pass);
                }
            } else {
                $correct = true;
            }
        } else {
            $error = "Заполните все обязательные поля";
            return $error;
        }
        if ($correct) {
            $this->model->updateUserData($idUser);
            $this->Success = "Успешно изменено";
        }
        return $error;
    }

    public function deleteUser($idUser)
    {

    }

    public function actionIndex()
    {
        $errorProfile = '';
        $idUser = $this->model->getIdUser($this->login);
        if (isset($_POST['change'])) {
            $errorProfile = $this->changeUserData($idUser);
        }
        if (isset($_POST['deleteButton'])) {
            if ($this->isAdmin()){
                $errorProfile = "Вы не можете удалить профиль администратора";
            }
            else {
                $this->model->deleteUser($idUser);
                session_unset();
                header('Location: index.php');
                exit();
            }
        }
        $this->getProfileData($idUser);
        return $this->render('profile', ['UserData' => $this->UserData, 'ErrorProfile' => $errorProfile, 'Success' => $this->Success], ['Title' => 'Профиль', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
    }
}