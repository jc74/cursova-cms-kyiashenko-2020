<?php
/**
 * Контролер сторінки корзина
 */

namespace controllers;


use core\Controller;

class Cart extends Controller
{
    protected $Cart = [];
    protected $BookId;

    public function getCss()
    {
        $this->css = "css/cartStyle.css";
    }

    public function changeCount($idUser, $idBook)
    {
        $this->model->updateCount($idUser, $idBook);
    }

    public function deleteBook($idUser, $idBook)
    {
        $this->model->deleteBookFromCart($idUser, $idBook);
    }

    public function actionIndex()
    {
        $this->BookId = $_GET['id'];
        $idUser = $this->model->getIdUser($this->login);
        if(isset($_POST['changeCount'])){
            $this->changeCount($idUser, $this->BookId);

        }
        if (isset($_POST['deleteBook'])){
            $this->deleteBook($idUser, $this->BookId);
        }
        $this->Cart = $this->model->getCart($idUser);
        return $this->render('cart', ['Cart' => $this->Cart], ['Title' => 'Корзина', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
    }
}