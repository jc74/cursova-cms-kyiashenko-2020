<?php
/**
 * Контролер головної сторінки
 */

namespace controllers;

use core\Controller;

class MainPage extends Controller
{
    public function getCss()
    {
        $this->css = "css/indexStyle.css";
    }

    public function actionIndex()
    {
        $newBooks = $this->model->getNewBooks();

        return $this->render('main', ['newBooks' => $newBooks], ['Title' => 'AlitLectio', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
    }
}