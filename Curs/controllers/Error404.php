<?php
/**
 * Контролер сторінки 404
 */

namespace controllers;


use core\Controller;

class Error404 extends Controller
{
    public function getCss()
    {
        $this->css = "css/error404Style.css";
    }
    public function ActionIndex()
    {
        return $this->render('error404', null, ['Title' => 'Ошибка 404', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
    }
}