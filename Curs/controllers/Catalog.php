<?php
/**
 * Контролер сторінки каталог
 */

namespace controllers;


use core\Controller;

class Catalog extends Controller
{
    protected $Books = [];
    protected $InsertedBook = [];
    protected $Success = '';

    public function getCss()
    {
        $this->css = "css/catalogStyle.css";
    }

    public function getCatalog()
    {
        $this->Books = $this->model->getBooks();
    }

    public function getCssForAdd()
    {
        $this->css = "css/bookStyle.css";
    }

    public function addBook()
    {
        $error = '';
        $this->InsertedBook['Bookname'] = $this->model->test_input($_POST['Bookname']);
        $this->InsertedBook['Author'] = $this->model->test_input($_POST['Author']);
        $this->InsertedBook['Price'] = $this->model->test_input($_POST['Price']);
        $this->InsertedBook['Description'] = $this->model->test_input($_POST['Description']);

        foreach ($this->InsertedBook as $value) {
            if ($value == '') {
                $error = "Заполните все обязательные поля";
                break;
            }
        }
        $img = $_FILES['Img']['name'];
        $this->error = $error;
        if ($error == '') {
            if ($img == '')
            {
                $img = 'no-img.jpg';
            }
            $this->model->insertBook($img);
            $this->Success = 'Успешно добавлено';
        }
    }

    public function actionIndex()
    {
        $this->getCatalog();
        if(isset($_POST['add'])){
            $this->getCssForAdd();
            return $this->render('addBook',null,  ['Title' => 'Добавление', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
        }
        if(isset($_POST['save']))
        {
            $this->addBook();
            $this->getCssForAdd();
            return $this->render('addBook',['Inserted' => $this->InsertedBook, 'Success' => $this->Success],  ['Title' => 'Добавление', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
        }
        return $this->render('catalog',['Books' => $this->Books, 'isAdmin' => $this->isAdmin()],  ['Title' => 'Каталог', 'EnterForm' => $this->enterForm, 'Error' => $this->error, 'Css' => $this->css]);
    }
}