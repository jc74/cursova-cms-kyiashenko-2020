<?php


namespace core;

/**
 * Базовий клас для всіх контролерів
 * @package core
 */
class Controller
{
    protected $model;
    protected $enterForm;
    protected $error;
    protected $css;
    protected $login;
    protected $RegExPass = "/[a-zA-Z\d]/";

    public function __construct()
    {
        $this->model = new \model\Model;
        $this->getEnterForm();
        $this->getCss();
    }

    public function getNotAuthorized()
    {
        $form = "<form class=\"form-inline my-2 my-lg-0\" method=\"post\" action=\"\">
            <input class=\"form-control mr-sm-2\" name=\"login\" type=\"text\" placeholder=\"Логин\">
            <input class=\"form-control mr-sm-2\" name=\"password\" type=\"password\" placeholder=\"Пароль\">
            <button class=\"btn btn-outline-primary my-2 my-sm-0\" type=\"submit\" name='enter'>Войти</button>
        </form>";
        return $form;
    }

    public function getAuthorized()
    {
        $form = "Привет,&nbsp<a href='profile' id='profile'> {$_SESSION['login']}</a>&nbsp
<a href='cart'><img src='images/shopping_cart_PNG3.png' width='50'></a>&nbsp&nbsp
    <form name='exit' method='post' action=''>
    <button class=\"btn btn-outline-primary my-2 my-sm-0\" type='submit' name='exit'>Выйти</button>
    </form>";
        return $form;
    }

    public function getEnterForm()
    {
        $error = '';
        $form = $this->getNotAuthorized();

        if (isset($_SESSION['login'])) {
            $form = $this->getAuthorized();
        }
        if (isset($_POST['enter'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $res = $this->model->checkAccount($login, $password);
            if ($res) {
                $_SESSION['login'] = $login;
                $form = $this->getAuthorized();
            } else {
                $error = "Неправильный логин или пароль";
            }
        }
        if (isset($_POST['exit'])) {
            session_unset();
            $form = $this->getNotAuthorized();
        }
        $this->enterForm = $form;
        $this->error = $error;
        $this->login = $_SESSION['login'];
    }

    public function isAdmin()
    {
        if ($this->login == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    public function render($viewName, $localParams = null, $globalParams = null)
    {
        $tpl = new Template();
        if (is_array($localParams)) {
            $tpl->setParams($localParams);
        }
        if (!is_array($globalParams)) {
            $globalParams = [];
        }
        $moduleName = strtolower((new \ReflectionClass($this))->getShortName());
        $globalParams['Content'] = $tpl->render("views/{$moduleName}/{$viewName}.php");
        return $globalParams;
    }
}