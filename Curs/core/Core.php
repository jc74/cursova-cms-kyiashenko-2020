<?php

namespace core;
/**
 * Головний клас ядра системи
 * @package core
 */
class Core
{
    private static $instance;
    private static $mainTemplate;

    private function __construct()
    {

    }

    /**
     * Повертає екземпляр ядра системи
     * @return Core
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Core();
            return self::getInstance();
        } else
            return self::$instance;
    }

    /**
     * Ініціалізація системи
     */
    public function init()
    {
        session_start();
        spl_autoload_register('\core\Core::my_autoload');
        self::$mainTemplate = new Template();
    }

    /**
     * Виконює основний процес роботи CMS системи
     */
    public function run()
    {
        $path = $_GET['path'];
        $pathParts = explode('/', $path);
        $className = ucfirst($pathParts[0]);
        if (empty($className)) {
            $fullClassName = 'controllers\\MainPage';
        } else {
            $fullClassName = 'controllers\\' . $className;
        }
        $methodName = ucfirst($pathParts[1]);
        if (empty($methodName)) {
            $fullMethodName = 'actionIndex';
        } else {
            $fullMethodName = 'action' . $methodName;
        }
        if (class_exists($fullClassName)) {
            $controller = new $fullClassName();
        } else {
            header("Location: Error404");
        }
        if (method_exists($controller, $fullMethodName)) {
            $method = new \ReflectionMethod($fullClassName, $fullMethodName);
            $paramsArray = [];
            foreach ($method->getParameters() as $parameter) {
                array_push($paramsArray, isset($_GET[$parameter->name]) ? $_GET[$parameter->name] : null);
            }
            $result = $method->invokeArgs($controller, $paramsArray);
            if (is_array($result)){
                self::$mainTemplate->setParams($result);
            }
        } else {
            header("Location: Error404");
        }
    }

    /**
     * Завершення роботи системи та виведення результату
     */
    public function done()
    {
        self::$mainTemplate->display('views/layout/index.php');
    }

    /**
     * Автозавантажувач класів
     * @param $className string Назва класу
     */
    public static function my_autoload($className)
    {
        $fileName = $className . '.php';
        if (is_file($fileName)) {
            include($fileName);
        }
    }
}