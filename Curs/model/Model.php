<?php
/**
 * Клас Model, виконує всю роботу з БД
 */

namespace model;


class Model
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = require('config.php');
    }

    function getNewBooks()
    {
        $select = $this->pdo->prepare('select * from books where type like "new"');
        $select->execute();
        $newBooks = $select->fetchAll();
        return $newBooks;
    }

    function pdoSet($allowed, &$values, $source = array())
    {
        $set = '';
        $values = array();
        if (!$source) $source = &$_POST;
        foreach ($allowed as $field) {
            if (isset($source[$field])) {
                $set .= "`" . str_replace("`", "``", $field) . "`" . "=:$field, ";
                $values[$field] = $source[$field];
            }
        }
        return substr($set, 0, -2);
    }

    public function checkAccount($login, $password)
    {
        $stmt = $this->pdo->prepare('select id from users where Login = ? and Password = ?');
        $stmt->execute(array($login, $password));
        $res = $stmt->fetch();
        return $res;
    }

    public function getUserByLogin($login)
    {
        $select = $this->pdo->prepare('select * from users where Login = ?');
        $select->execute(array($login));
        $resLog = $select->fetch(\PDO::FETCH_LAZY);
        return $resLog;
    }

    public function registration($regData)
    {
        $correct = true;
        $error = '';
        $resLog = $this->getUserByLogin($regData['login']);
        if ($resLog) {
            $correct = false;
            $error = "Пользователь с таким логином уже есть";
        }
        if ($correct) {
            $allowed = array("Name", "Surname", "EMail", "Country", "Login", "Password", "Date_of_birth", "Sex", "Hobbies");
            $insert = $this->pdo->prepare("INSERT INTO users SET " . $this->pdoSet($allowed, $values));
            $insert->execute($values);
        }
        return $error;
    }

    public function getBooks()
    {
        $select = $this->pdo->prepare('select * from books');
        $select->execute();
        $books = $select->fetchAll();
        return $books;
    }

    public function getBook($idBook)
    {
        $select = $this->pdo->prepare('select * from books where id_book = ?');
        $select->execute(array($idBook));
        $book = $select->fetch();
        return $book;
    }

    public function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function getIdUser($login)
    {
        $selectUser = $this->pdo->prepare('select * from users where Login = ?');
        $selectUser->execute(array($login));
        $resUser = $selectUser->fetch(\PDO::FETCH_LAZY);
        $id = $resUser[0];
        return $id;
    }

    public function addToCart($idUser, $idBook)
    {
        $selectCart = $this->pdo->prepare('select * from cart where id_user = :id_user and id_book = :id_book');
        $selectCart->execute(array('id_user' => $idUser, 'id_book' => $idBook));
        $resCart = $selectCart->fetch();
        $countOrder = $_POST['countOrder'];
        if ($resCart) {
            $count = $resCart['Count'];
            $updateCart = $this->pdo->prepare('update cart set Count = :Count where id_user = :id_user and id_book = :id_book');
            $count += $countOrder;
            $updateCart->execute(array('Count' => $count, 'id_user' => $idUser, 'id_book' => $idBook));
        } else {
            $insertCart = $this->pdo->prepare('insert into cart (id_book, Count, id_user) values (:id_book, :count, :id_user)');
            $insertCart->execute(array('id_user' => $idUser, 'count' => $countOrder, 'id_book' => $idBook));
        }
    }

    public function updateBook($idBook)
    {
        $allowed = array("Bookname", "Author", "Description", "Price");
        $update = $this->pdo->prepare("UPDATE books SET " . $this->pdoSet($allowed, $values) . " WHERE id_book = :id_book");
        $values["id_book"] = $idBook;
        $update->execute($values);
    }

    public function updateImage($idBook)
    {
        $imgSrc = "images/books/".$_FILES['Img']['name'];
        $updateImg = $this->pdo->prepare('UPDATE books SET Img = :Img WHERE  id_book = :id_book');
        $updateImg->execute(array('Img' => $imgSrc, 'id_book' => $idBook));
    }

    public function deleteBook($idBook)
    {
        $delete = $this->pdo->prepare('delete from books where id_book = ?');
        $delete->execute(array($idBook));
    }

    public function insertBook($img)
    {
        $allowed = array("Bookname", "Author", "Description", "Price");
        $insert = $this->pdo->prepare("INSERT INTO books SET " . $this->pdoSet($allowed, $values). ", Img = :Img");
        $values["Img"] = 'images/books/'.$img;
        $insert->execute($values);
    }

    public function getCart($idUser)
    {
        $select = $this->pdo->prepare('select books.*, cart.Count from cart join books on cart.id_book = books.id_book where id_user = ?');
        $select->execute(array($idUser));
        $resCart = $select->fetchAll();
        return $resCart;
    }

    public function updateCount($idUser, $idBook)
    {
        $update = $this->pdo->prepare('update cart set Count = :count where id_user = :user and id_book = :book');
        $update->execute(array('count' => $_POST['count'], 'user' => $idUser, 'book' => $idBook));
    }

    public function deleteBookFromCart($idUser, $idBook)
    {
        $delete = $this->pdo->prepare('delete from cart where id_user = ? and id_book = ?');
        $delete->execute(array($idUser, $idBook));
    }

    public function getUserData($idUser)
    {
        $select = $this->pdo->prepare('select * from users where id = ?');
        $select->execute(array($idUser));
        $res = $select->fetch(\PDO::FETCH_LAZY);
        return $res;
    }

    public function updateUserData($idUser)
    {
        $allowed = array("Name", "Surname", "EMail", "Country", "Login", "Date_of_Birth", "Sex", "Hobbies");
        $update = $this->pdo->prepare("UPDATE users SET " . $this->pdoSet($allowed, $values) . " WHERE id = :id");
        $values["id"] = $idUser;
        $update->execute($values);
    }

    public function updatePassword($idUser, $pass)
    {
        $updatePass = $this->pdo->prepare("UPDATE users SET Password = ? WHERE id = ?");
        $updatePass->execute(array($pass, $idUser));
    }

    public function deleteUser($idUser)
    {
        $delete = $this->pdo->prepare('delete from users where id = ?');
        $delete->execute(array($idUser));
    }
}