<?php
/**
 * Налаштування підключення до БД, дані зберігаються в JSON
 */
$json = file_get_contents('js/config.json');
$jsonDecode = json_decode($json,true);
$host = $jsonDecode['host'];
$db = $jsonDecode['db'];
$user = $jsonDecode['user'];
$pass = $jsonDecode['password'];
$charset = $jsonDecode['charset'];

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
return $pdo;